'use client';

import { FormEvent, useEffect, useState, useRef } from 'react';
import axios from 'axios';
import { io } from 'socket.io-client';

import styles from './page.module.css';

// In bigger scale apps - enums + types would be in seperate directories
enum eMessageStatuses {
    Success = 1,
    Failure,
    Pending,
}
type Message = {
    text: string;
    status: eMessageStatuses;
    key?: string;
};

export default function Home() {
    const [messages, setMessages] = useState<Message[]>([]);
    const [currText, setCurrText] = useState('');
    const messagesRef = useRef<Message[]>();
    messagesRef.current = messages;

    useEffect(() => {
        // In bigger scale apps - io socket will be in context
        const socket = io(process.env.NEXT_PUBLIC_SERVER_URL || '');
        socket.on('new_status', (msgData: Message) => {
            console.log('current', messagesRef.current);
            messagesRef.current &&
                setMessages(
                    messagesRef.current.map((msg: Message) =>
                        msg.key === msgData.key
                            ? { ...msg, status: msgData.status }
                            : msg
                    )
                );
        });
    }, []);

    const msgStatusToText = {
        [eMessageStatuses.Success]: 'success',
        [eMessageStatuses.Failure]: 'failure',
        [eMessageStatuses.Pending]: 'pending',
    };

    const handleSubmit = async (e: FormEvent) => {
        e.preventDefault();
        if (!currText) return;
        const newMessage = await postNewMessage({
            text: currText,
            status: eMessageStatuses.Pending,
        });
        setMessages((messages) => [...messages, newMessage]);
        setCurrText('');
    };

    // In any bigger scale app - api should be in seperate service
    const postNewMessage = async (message: Message): Promise<Message> => {
        try {
            const key = (
                await axios.post(
                    `${process.env.NEXT_PUBLIC_SERVER_URL}/messages`,
                    {
                        message: message.text,
                    }
                )
            ).data;
            return { ...message, key };
        } catch (e) {
            return { ...message, status: eMessageStatuses.Failure };
        }
    };

    return (
        <div className={styles.homeContainer}>
            <div>
                <form onSubmit={(e) => handleSubmit(e)}>
                    <input
                        value={currText}
                        onChange={(e) => setCurrText(e.target.value)}
                    />
                    <input type="submit" value="submit" />
                    <ul>
                        {messages.map((message, index) => (
                            <li key={index}>
                                {message.text} {msgStatusToText[message.status]}
                            </li>
                        ))}
                    </ul>
                </form>
            </div>
        </div>
    );
}
